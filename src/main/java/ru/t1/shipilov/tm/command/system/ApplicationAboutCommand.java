package ru.t1.shipilov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    private final String NAME = "about";

    @NotNull
    private final String DESCRIPTION = "Show developer info.";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexey Shipilov");
        System.out.println("E-mail: gollarx@gmail.com");
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
