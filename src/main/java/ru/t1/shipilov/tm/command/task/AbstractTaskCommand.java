package ru.t1.shipilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shipilov.tm.api.service.IProjectTaskService;
import ru.t1.shipilov.tm.api.service.ITaskService;
import ru.t1.shipilov.tm.command.AbstractCommand;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTasks(@NotNull final List<Task> tasks) {
        int index = 1;
        for (@Nullable Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName() + ": " + task.getDescription() + ", "
                    + Status.toName(task.getStatus()));

            index++;
        }
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
