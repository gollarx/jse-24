package ru.t1.shipilov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private final String ARGUMENT = "-v";

    @NotNull
    private final String NAME = "version";

    @NotNull
    private final String DESCRIPTION = "Show version info.";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.24.1");
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
