package ru.t1.shipilov.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.shipilov.tm.api.repository.ICommandRepository;
import ru.t1.shipilov.tm.api.repository.IProjectRepository;
import ru.t1.shipilov.tm.api.repository.ITaskRepository;
import ru.t1.shipilov.tm.api.repository.IUserRepository;
import ru.t1.shipilov.tm.api.service.*;
import ru.t1.shipilov.tm.command.AbstractCommand;
import ru.t1.shipilov.tm.enumerated.Role;
import ru.t1.shipilov.tm.enumerated.Status;
import ru.t1.shipilov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.shipilov.tm.exception.system.CommandNotSupportedException;
import ru.t1.shipilov.tm.model.Project;
import ru.t1.shipilov.tm.model.User;
import ru.t1.shipilov.tm.repository.CommandRepository;
import ru.t1.shipilov.tm.repository.ProjectRepository;
import ru.t1.shipilov.tm.repository.TaskRepository;
import ru.t1.shipilov.tm.repository.UserRepository;
import ru.t1.shipilov.tm.service.*;
import ru.t1.shipilov.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.shipilov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private boolean processArgument(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length < 1) return false;
        processArgument(arguments[0]);
        return true;
    }

    private void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void initDemoData() {
        @NotNull final User test = userService.create("test", "test", "test@gmail.com");
        @NotNull final User user = userService.create("user", "user", "user@ya.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(test.getId(), new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("TEST PROJECT", Status.NOT_STARTED));
        projectService.add(test.getId(), new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("TEST PROJECT", Status.COMPLETED));
        taskService.create(test.getId(), "MEGA TASK");
        taskService.create(test.getId(), "BETA PROJECT");
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) System.exit(0);

        initLogger();
        initDemoData();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
